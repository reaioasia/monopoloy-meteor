import React from 'react';
import {mount} from 'react-mounter';

import {PlayerLayout} from './../../imports/client/layouts/PlayerLayout.jsx';
import {ZonePage} from './../../imports/client/pages/ZonePage.jsx';
import {AreaPage} from './../../imports/client/pages/AreaPage.jsx';
import {LandPage} from './../../imports/client/pages/LandPage.jsx';
import {MarketplacePage} from './../../imports/client/pages/MarketplacePage.jsx';
import {PlayerPorfolioPage} from './../../imports/client/pages/PlayerPorfolioPage.jsx';
import {Stash} from './../../imports/client/pages/Stash.jsx';
import {Notifier} from './../../imports/common/Notifier.js';

let notifierInstance = new Notifier();

FlowRouter.route('/stash',{
  action(){
      mount(PlayerLayout,{
        content: (<Stash gold={1000} gems={2} techPoints={0} distance={5000} nickname={'HansSolo'} notifier={notifierInstance} />)
      })
  }
});

FlowRouter.route('/map/zone',{
  action(){
    mount(PlayerLayout,{
      content: (<ZonePage />)
    });
  }
});

FlowRouter.route('/map/land',{
  action(){
    mount(PlayerLayout,{
      content: (<LandPage />)
    });
  }
});

FlowRouter.route('/map/area',{
  action(){
    mount(PlayerLayout,{
      content: (<AreaPage />)
    });
  }
});

FlowRouter.route('/marketplace',{
  action(){
    mount(PlayerLayout,{
      content: (<MarketplacePage notifier={notifierInstance} />)
    });
  }
});

FlowRouter.route('/me/portfolio',{
  action(){
    mount(PlayerLayout,{
      content: (<PlayerPorfolioPage notifier={notifierInstance} />)
    });
  }
});
