import React from 'react';
import {mount} from 'react-mounter';

import {PublicLayout} from './../../imports/client/layouts/PublicLayout.jsx';
import {LoginForm} from './../../imports/client/components/LoginForm.jsx';
import {PublicPage} from './../../imports/client/pages/PublicPage.jsx';

FlowRouter.route('/',{
  action(){
      mount(PublicLayout,{
        content: (<PublicPage />)
      })
  }
});