$(document).ready(function(){

    // Bottom Menu
    $('#rightMenu').click(function() {
        $('div.overlayRightMenu').css("display", "block");
    });

    $('.overlayRightMenu').click(function() {
        $('div.overlayRightMenu').css("display", "none");
    });

    $('#leftMenu').click(function() {
        $('div.overlayLeftMenu').css("display", "block");
    });

    $('.overlayLeftMenu').click(function() {
        $('div.overlayLeftMenu').css("display", "none");
    });

    // Pop Up Close
    $('.popClose, #btnBuy').click(function() {
        $('div.overlayPopUp, div.overlayPopUpOwner, div.overlayPopUpPurchase, div.overlayPopUpPurchaseDone').css("display", "none");
    });

    // Pop Up
    $('#btnConvert').click(function() {
        $('div.overlayPopUp').css("display", "block");
    });

    $('#btnPop').click(function() {
        $('div.overlayPopUp').css("display", "block");
    });

    // Pop Up Elec & Water Timer
    $('.hexWater, .hexElec').click(function() {
        $('div.counterBgTimer').css("display", "block");
    });

    // Pop Up Move
    $('.selectTransport').click(function() {
        $('div.counterBgMove').css("display", "block");
    });

    $('.counterBgTimer').click(function() {
        $('div.counterBgTimer').css("display", "none");
    });

    // Pop Up Owner
    $('.hexMine, .hexNotMine').click(function() {
        $('div.overlayPopUpOwner').css("display", "block");
    });

    // Pop Up Purchase
    $('.hexEmpty').click(function() {
        $('div.overlayPopUpPurchase').css("display", "block");
    });

     // Pop Up Purchase
    $('#btnBuy').click(function() {
        $('div.overlayPopUpPurchaseDone').css("display", "block");
    });

});

 // Tab Content //
$('ul.tabs li').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#"+tab_id).addClass('current');
})

// Redirect Link //
function redirect() {
	window.location = "zone.html"
}