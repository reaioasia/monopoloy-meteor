import React from 'react';

export const MemberLayout = ({content}) =>(
    <div className='member'>
      {content}
    </div>
)
