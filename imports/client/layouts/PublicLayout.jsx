import React from 'react';

export const PublicLayout = ({content}) =>(
    <div className='public'>
      {content}
    </div>
)
