import React from 'react';
import ReactDOM,{render} from 'react-dom';
import geolib from 'geolib';
//import {Meteor} from 'meteor/meteor';

const refreshRate = 3000;   //3000ms
const distToGoldRate = 50;    //1km = 50 gold
const speedThreashold = 40;
const options = {
  enableHighAccuracy: true,
  timeout: 50000,
  maximumAge: 0
};


export class Stash extends React.Component{
  constructor(props){
    super(props);
    console.log(props.notifier);
    this._fetchReading = this._fetchReading.bind(this);
    this._computeDistance = this._computeDistance.bind(this);
    this._mineGoldFromDistance = this._mineGoldFromDistance.bind(this);
    this._getSpeed = this._getSpeed.bind(this);
    this._convertToGold = this._convertToGold.bind(this);
    this.state = {
        prevLoc: {latitude: 0, longitude: 0},
        currLoc: {latitude: 0, longitude: 0},
        time: 0,
        distance: props.distance,   //in metre
        speed: 0,
        gold: props.gold,    //initial gold
        gems: props.gems,
        techPoints: props.techPoints
    };
  }
  componentWillMount(){
      setInterval(this._fetchReading, refreshRate); //capture distance every 1000ms
  }
  componentWillUnmount(){
      clearInterval(this._fetchReading);
  }

  _computeDistance(locA, locB){
    console.log("debug-computing distance");
    let dist = geolib.getDistance(locA, locB);
    return dist;
  }

  _getSpeed(locA, locB, timeA, timeB){
      console.log("debug-calculating speed");
      //console.log(locB.latitude);
      return geolib.getSpeed(
          {lat: locA.latitude, lng: locA.longitude, time: timeA},
          {lat: locB.latitude, lng: locB.longitude, time: timeB});
  }

  _mineGoldFromDistance(dist){
      console.log("debug-mining gold");
     let gold = Math.round(dist / 1000)  * distToGoldRate;  //convert metre to km first only multiply
     //let gold = dist   * distToGoldRate;
     return gold;
  }

  _fetchReading(){
         console.log("debug-fetching reading");
         if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                if (position === null || position.coords == null){
                    console.log('debug-position is null');
                    return;
                }
                console.log('debug-longitude: ' + position.coords.longitude);
                console.log('debug-latitude: ' + position.coords.latitude);
                let d = new Date();
                let newState = Object.assign(this.state, {
                    prevLoc: this.state.currLoc,
                    currLoc: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    },
                    time: d.getTime()   //current time in ms
                });

                let speed = this._getSpeed(this.state.prevLoc, this.state.currLoc, (this.state.time - refreshRate), this.state.time);
                console.log('debug-speed: ' + speed);

                if (speed < speedThreashold){
                    let dist = this._computeDistance(this.state.prevLoc, this.state.currLoc);
                    console.log('debug-delta distance: ' + dist);
                    this.setState(Object.assign(this.state,{
                            speed,
                            distance: this.state.distance + (dist / 1000)
                        }));
                }
            }, (err) => {
                console.log(err);
            }, options);
        } else {
            console.log("Geolocation is not supported by this browser.");
        }
  }
  _convertToGold(){
    let distance = this.state.distance;
    console.log('debug-distance accummulated: ' + distance)
    let gold = this._mineGoldFromDistance(distance);
        console.log('debug-gold: ' + gold);
        this.setState(Object.assign(this.state, {
            gold: this.state.gold + gold,
            distance: 0 //reset after conversion
            //speed
        }));
  }
  render(){
      return (
         <div className="StashContainer" style={{flexDirection: 'column'}}>
            <div className="topPanel">
                <div className="topAreaLeft">
                    <a href="index.html" /><img src="/images/icon-button-back.svg" alt="" />
                </div>
                <div className="topAreaCenter">
                    <div className="title">STEPS</div>
                </div>
                <div className="topAreaRight">
                    <a href="steps.html" /><img src="/images/icon-jogging.svg" alt="" />
                </div>
            </div>
            <div className="currentPointBar">
                <ul>
                    <li><img src="/images/icon-goldPoint.svg" alt="" /> <span>{this.state.gold}</span></li>
                    <li><img src="/images/icon-techPoint.svg" alt="" /> <span>999,999</span></li>
                </ul>
            </div>
            <div className="container">
                <div className="distanceCounter">
                    <div className="meter wow fadeInDown" data-wow-delay="0.2s">
                    <h4>Total</h4>
                    <h2>{this.state.distance / 1000} <small>KM</small></h2>
                </div>
                <div className="description wow fadeInUp" data-wow-delay="0.3s">1KM = 50 Gold</div>
                <button type="button" className="wow fadeInUp" data-wow-delay="0.4s" id="btnConvert" onClick={this._convertToGold}>Convert</button>
            </div>

            </div>

            <div className="overlayPopUp">
                    <div className="popBody">
                        <div className="popHeader">
                            <div className="popTitle">Tech Point</div>
                            <div className="popClose"><img src="/images/icon-close.svg" alt="" /></div>
                        </div>
                        <div className="popContent">
                            <h2>You Just Earned</h2>
                            <h3>1 Tech Point</h3>
                        </div>
                        <div className="popFooter"></div>
                    </div>
                </div>
        </div>
      );
  }
}
