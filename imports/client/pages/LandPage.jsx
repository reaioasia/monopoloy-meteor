import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

import {CommonHeader} from './../components/CommonHeader.jsx';
import {LandMap} from './../components/LandMap.jsx';
import {LandPlayerPop} from './../components/LandPlayerPop.jsx';
import {LandPowerPop} from './../components/LandPowerPop.jsx';
import {LandBuyPop} from './../components/LandBuyPop.jsx';
import {LandBuyDonePop} from './../components/LandBuyDonePop.jsx';
import {ZoneFooter} from './../components/ZoneFooter.jsx';

export class LandPage extends React.Component{
  constructor(){
    super();

    this.state = {
      showPlayerPop: false,
      showPowerPop: false,
      showBuyPop: false,
      showBuyDonePop: false
    }
  }

  togglePlayerPop(){
    if(this.state.showPlayerPop){
      this.setState({
        showPlayerPop: false
      })
    }else{
      this.setState({
        showPlayerPop: true
      })
    }
  }

  togglePowerPop(){
    if(this.state.showPowerPop){
      this.setState({
        showPowerPop: false
      })
    }else{
      this.setState({
        showPowerPop: true
      })
    }
  }

  toggleBuyPop(){
    if(this.state.showBuyPop){
      this.setState({
        showBuyPop: false
      })
    }else{
      this.setState({
        showBuyPop: true
      })
    }
  }

  toggleBuyDonePop(){
    if(this.state.showBuyDonePop){
      this.setState({
        showBuyDonePop: false
      })
    }else{
      this.setState({
        showBuyDonePop: true
      })
    }
  }

  render(){
    let _playerPop = '';

    if(this.state.showPlayerPop){
      _playerPop = (<LandPlayerPop togglePlayerPop={this.togglePlayerPop.bind(this)}/>);
    }else{
      _playerPop = '';
    }

    let _powerPop = '';

    if(this.state.showPowerPop){
      _powerPop = (<LandPowerPop togglePowerPop={this.togglePowerPop.bind(this)}/>);
    }else{
      _powerPop = '';
    }

    let _buyPop = '';

    if(this.state.showBuyPop){
      _buyPop = (<LandBuyPop toggleBuyPop={this.toggleBuyPop.bind(this)} showBuyDonePop={this.toggleBuyDonePop.bind(this)}/>);
    }else{
      _buyPop = '';
    }

    let _buyDonePop = '';

    if(this.state.showBuyPop){
      _buyDonePop = (<LandBuyDonePop toggleBuyDonePop={this.toggleBuyDonePop.bind(this)} />);
    }else{
      _buyDonePop = '';
    }

    return(
      <div className='player-page'>
        
        <CommonHeader hideSub={true} headerLabel='Area'/>
        <LandMap showPlayerPop={this.togglePlayerPop.bind(this)} showPowerPop={this.togglePowerPop.bind(this)}
        showBuyPop={this.toggleBuyPop.bind(this)} />

        {_playerPop}
        {_powerPop}
        {_buyPop}
        {_buyDonePop}
        <ZoneFooter />
      </div>
    )
  }
}
