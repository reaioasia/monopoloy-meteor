import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

import {LoginForm} from './../components/LoginForm.jsx';

export class PublicPage extends React.Component{
  constructor(){
    super();
  }

  render(){
    return(
      <LoginForm />
    )
  }
}
