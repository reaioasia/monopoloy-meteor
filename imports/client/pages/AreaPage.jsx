import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

import {CommonHeader} from './../components/CommonHeader.jsx';

export class AreaPage extends React.Component{
  constructor(){
    super();
  }

  render(){
    return(
      <div className='player-page'>
        <CommonHeader hideSub={true} headerLabel='Area'/>
      </div>
    )
  }
}
