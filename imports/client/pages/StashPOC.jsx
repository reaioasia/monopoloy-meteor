import React from 'react';
import ReactDOM,{render} from 'react-dom';
//import {geolocated, geoPropTypes} from 'react-geolocated';
import geolib from 'geolib';
//import {Meteor} from 'meteor/meteor';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';

const refreshRate = 1000;   //1000ms
const distToGoldRate = 50;    //1km = 50 gold
const speedThreashold = 40;
const options = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 0
};


export default class Stash extends React.Component{
  constructor(props){
    super(props);
    this._fetchReading = this._fetchReading.bind(this);
    this._computeDistance = this._computeDistance.bind(this);
    this._mineGoldFromDistance = this._mineGoldFromDistance.bind(this);
    this._getSpeed = this._getSpeed.bind(this);
    this.state = {
        //reading: null
        prevLoc: {latitude: 0, longitude: 0},
        currLoc: {latitude: 0, longitude: 0},
        time: 0,
        distance: 0,
        speed: 0,
        gold: props.gold,    //initial gold
        gems: props.gems,
        techPoints: props.techPoints
    };
  }
  componentWillMount(){
      setInterval(this._fetchReading, refreshRate); //capture distance every 1000ms
  }
  componentWillUnmount(){
      clearInterval(this._fetchReading);
  }

  _computeDistance(locA, locB){
    console.log("debug-computing distance");
    let dist = geolib.getDistance(locA, locB);
    return dist;
  }

  _getSpeed(locA, locB, timeA, timeB){
      console.log("debug-calculating speed");
      console.log(locB.latitude);
      return geolib.getSpeed(
          {lat: locA.latitude, lng: locA.longitude, time: timeA},
          {lat: locB.latitude, lng: locB.longitude, time: timeB});
  }

  _mineGoldFromDistance(dist){
      console.log("debug-mining gold");
     let gold = Math.round(dist / 1000)  * distToGoldRate;  //convert metre to km first only multiply
     return gold;
  }

  _fetchReading(){
         console.log("debug-fetching reading");
         if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                if (position === null || position.coords == null){
                    console.log('debug-position is null');
                    return;
                }
                console.log('debug-longitude: ' + position.coords.longitude);
                console.log('debug-latitude: ' + position.coords.latitude);
                let d = new Date();
                let newState = Object.assign(this.state, {
                    prevLoc: this.state.currLoc,
                    currLoc: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    },
                    time: d.getTime()   //current time in ms
                });
                let dist = this._computeDistance(this.state.prevLoc, this.state.currLoc);
                console.log('debug-delta distance: ' + dist);
                this.setState(Object.assign(this.state,{
                        distance: this.state.distance + (dist / 1000)
                    }));
                let speed = this._getSpeed(this.state.prevLoc, this.state.currLoc, (this.state.time - refreshRate), this.state.time);
                console.log('debug-speed: ' + speed);
                if (speed < speedThreashold){
                    let gold = this._mineGoldFromDistance(dist);
                    console.log('debug-gold: ' + gold);
                    this.setState(Object.assign(this.state, {
                        gold: this.state.gold + gold,
                        speed
                    }));
                }
            }, (err) => {
                console.log(err);
            }, options);
        } else { 
            console.log("Geolocation is not supported by this browser.");
        }
  }
  render(){
      return (  //TODO: please think of a way on how to link these variables to 'View' and also how to construct components
         <div className="StashContainer" style={{flexDirection: 'column'}}>
              <div>
                Distance Travelled: {this.state.distance / 1000} km
                <br/>
                Current Speed: {this.state.speed} kph
                <br/>
                Gold: {this.state.gold}
                <br/>
                Gem: {this.state.gems}
                <br/>
                Tech Points: {this.state.techPoints}
            </div> 
        </div>
      );
  }
}
