import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

import {CommonHeader} from './../components/CommonHeader.jsx';
import {ZoneMap} from './../components/ZoneMap.jsx';

export class ZonePage extends React.Component{
  constructor(){
    super();
  }

  render(){
    return(
      <div className='player-page'>
        <CommonHeader hideSub={true} headerLabel='Zone'/>
        <ZoneMap />
      </div>
    )
  }
}
