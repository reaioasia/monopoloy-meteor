import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

import {CommonHeader} from './../components/CommonHeader.jsx';
import {CommonTabs} from './../components/CommonTabs.jsx';
import {MarketplaceContent} from './../components/MarketplaceContent.jsx';

export class MarketplacePage extends React.Component{
  constructor(props){
    super(props);
    console.log(props.notifier);
    this.state = {
      currentTab: 'transportation'
    };
  }

  updateCurrentTab(val,event){
    console.log(val);
    this.setState({
      currentTab: val
    })
  }

  render(){
      return(
        <div className='player-page'>
          <CommonHeader headerLabel='Marketplace' notifier={this.props.notifier}/>
          <CommonTabs updateCurrentTab={this.updateCurrentTab.bind(this)} notifier={this.props.notifier}/>
          <MarketplaceContent currentTab={this.state.currentTab} notifier={this.props.notifier}/>
        </div>
      )
  }
}
