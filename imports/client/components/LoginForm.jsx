import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

export class LoginForm extends React.Component{
  constructor(){
    super();
  }

  doLogin(){
    FlowRouter.go('/map/zone');
  }

  render(){
    return(
      <div className='account-form'>
        <div className='container'>
          <header>
            <img src='/images/logo-property-master-white.svg' />
          </header>

          <form>
            <label>Email Address</label>
            <input type='text' ref='email' />

            <label>Password</label>
            <input type='password' ref='password' />

            <button onClick={this.doLogin.bind(this)}>Login</button>
          </form>
        </div>
      </div>
    );
  }
}
