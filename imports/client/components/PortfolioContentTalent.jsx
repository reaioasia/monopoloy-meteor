import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

const talentData = [
  {
    icon: '/images/avatar-han-solo.svg',
    label: 'Han Solo',
    description: '2 Land Move / 1 Area Move',
    count: 1
  },
  {
    icon: '/images/avatar-han-solo.svg',
    label: 'Han Solo',
    description: '2 Land Move / 1 Area Move',
    count: 1
  },
  {
    icon: '/images/avatar-han-solo.svg',
    label: 'Han Solo',
    description: '2 Land Move / 1 Area Move',
    count: 1
  },
  {
    icon: '/images/avatar-han-solo.svg',
    label: 'Han Solo',
    description: '2 Land Move / 1 Area Move',
    count: 1
  }
];

export class PortfolioContentTalent extends React.Component{
  constructor(){
    super();
  }

  render(){
    let _talents = [];

    talentData.forEach((val,key)=>{
      console.log(val);
      let _output = (
        <li>
          <section className='icon'>
            <img src={val.icon} />
          </section>
          <section className='description'>
            <h2>{val.label}</h2>
            <h3>{val.description}</h3>
          </section>
          <section className='btnPurchase'><h2>X {val.count}</h2></section>
        </li>
      );

      _talents.push(_output);
    })
    return(
      <div className='marketplace-content fadeInUp animated'>
        <ul>
          {_talents}
        </ul>
      </div>
    )
  }
}
