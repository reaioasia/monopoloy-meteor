import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

export class ZoneMap extends React.Component{
  constructor(){
    super();

  }

  goToLand(){
    FlowRouter.go('/map/land');
  }

  render(){
    return(
      <div className="gridContainer wow fadeIn animated" data-wow-delay="0.2s">
        <ul id="hexGrid" onClick={this.goToLand.bind(this)}>
            <li className="hex">
                <div className="hexIn">
                    <a className="hexLink hexArea" href="land.html">
                        <h2>Klang</h2>
                    </a>
                </div>
            </li>
            <li className="hex">
                <div className="hexIn">
                    <a className="hexLink hexArea" href="land.html">
                        <h2>Ampang Jaya</h2>
                    </a>
                </div>
            </li>
            <li className="hex">
                <div className="hexIn">
                    <a className="hexLink hexArea" href="land.html">
                        <h2>Subang Jaya</h2>
                    </a>
                </div>
            </li>
            <li className="hex">
                <div className="hexIn">
                    <a className="hexLink hexZone" href="land.html">
                        <h2>Shah Alam</h2>
                    </a>
                </div>
            </li>
            <li className="hex">
                <div className="hexIn">
                    <a className="hexLink hexZone" href="land.html">
                        <h2>Petaling Jaya</h2>
                    </a>
                </div>
            </li>
            <li className="hex">
                <div className="hexIn">
                    <a className="hexLink hexZone" href="land.html">
                        <h2>Cheras</h2>
                    </a>
                </div>
            </li>
            <li className="hex">
                <div className="hexIn">
                    <a className="hexLink hexZone" href="land.html">
                        <h2>Kajang</h2>
                    </a>
                </div>
            </li>
            <li className="hex">
                <div className="hexIn">
                    <a className="hexLink hexZone" href="land.html">
                        <h2>Selayang Baru</h2>
                    </a>
                </div>
            </li>
            <li className="hex">
                <div className="hexIn">
                    <a className="hexLink hexZone" href="land.html">
                        <h2>Rawang</h2>
                    </a>
                </div>
            </li>
            <li className="hex">
                <div className="hexIn">
                    <a className="hexLink hexZone" href="land.html">
                        <h2>Taman Greenwood</h2>
                    </a>
                </div>
            </li>
        </ul>
    </div>
    );
  }
}
