import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

import {MarketplaceContentTalent} from './MarketplaceContentTalent.jsx';
import {MarketplaceContentBuilding} from './MarketplaceContentBuilding.jsx';
import {MarketplaceContentTransportation} from './MarketplaceContentTransportation.jsx';

export class MarketplaceContent extends React.Component{
  constructor(props){
    super(props);
    console.log('MarketplaceContent:' + props.notifier);
  }

  render(){
    switch (this.props.currentTab) {
      case 'transportation':
      return (<MarketplaceContentTransportation notifier={this.props.notifier} />);
      case 'building':
      return (<MarketplaceContentBuilding notifier={this.props.notifier} />);
      case 'talent':
      return (<MarketplaceContentTalent notifier={this.props.notifier} />);
      default:
      return (<MarketplaceContentTransportation notifier={this.props.notifier} />);
    }
  }
}
