import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

const transportationData = [
  {
    icon: '/images/icon-p-bike.svg',
    label: 'Bicycle',
    description: '2 Land Move / 1 Area Move',
    price: 500
  },
  {
    icon: '/images/icon-p-bike.svg',
    label: 'Bicycle',
    description: '2 Land Move / 1 Area Move',
    price: 500
  },
  {
    icon: '/images/icon-p-bike.svg',
    label: 'Bicycle',
    description: '2 Land Move / 1 Area Move',
    price: 500
  },
  {
    icon: '/images/icon-p-bike.svg',
    label: 'Bicycle',
    description: '2 Land Move / 1 Area Move',
    price: 500
  }
];

export class MarketplaceContentTransportation extends React.Component{
  constructor(props){
    super(props);
    this._Purchase = this._Purchase.bind(this);
    console.log('MarketplaceContentTransportation: ' + props.notifier);
  }

  _Purchase(price){
    console.log('Purchase');
    this.props.notifier.publish('GOLD_UPDATED', -price);
    this.props.notifier.publish('BICYCLE_UPDATED', 1);
  }

  render(){
    let _transportations = [];

    transportationData.forEach((val,key)=>{
      console.log(val);
      let _output = (
        <li>
          <section className='icon'>
            <img src={val.icon} />
          </section>
          <section className='description'>
            <h2>{val.label}</h2>
            <h3>{val.description}</h3>
          </section>
          <section className='btnPurchase'><button onClick={()=> {this._Purchase(val.price);}}>{val.price}</button></section>
        </li>
      );

      _transportations.push(_output);
    })
    return(
      <div className='marketplace-content fadeInUp animated'>
        <ul>
          {_transportations}
        </ul>
      </div>
    )
  }
}
