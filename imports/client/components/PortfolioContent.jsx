import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

import {PortfolioContentTalent} from './PortfolioContentTalent.jsx';
import {PortfolioContentBuilding} from './PortfolioContentBuilding.jsx';
import {PortfolioContentTransportation} from './PortfolioContentTransportation.jsx';

export class PortfolioContent extends React.Component{
  constructor(props){
    super(props);
    console.log('PortfolioContent' + props.notifier)
    console.log(PortfolioContentTransportation)
  }

  render(){
    switch (this.props.currentTab) {
      case 'transportation':
      return (<PortfolioContentTransportation notifier={this.props.notifier} />);
      case 'building':
      return (<PortfolioContentBuilding notifier={this.props.notifier} />);
      case 'talent':
      return (<PortfolioContentTalent notifier={this.props.notifier} />);
      default:
      console.log('testing');
      return (<PortfolioContentTransportation notifier={this.props.notifier} />);
    }
  }
}
