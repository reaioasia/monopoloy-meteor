import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

export class LandBuyDonePop extends React.Component{
  constructor(){
    super();
  }

  render(){
    return(
      <div className="overlayPopUpPurchaseDone wow fadeIn" data-wow-delay="0.1s">
          <div className="popBody">
              <div className="popHeader">
                  <div className="popTitle">Purchase</div>
                  <div className="popClose"><img src="/images/icon-close.svg" /></div>
              </div>
              <div className="popContent">
                  <div className="info">
                      <h1>Congratulations!!!</h1>
                      <h2>You Just Purchased A Land Worth</h2>
                      <h3>200,000 Gold</h3>
                  </div>
              </div>
              <div className="popFooter"></div>
          </div>
      </div>
    )
  }
}
