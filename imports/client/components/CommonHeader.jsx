import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

export class CommonHeader extends React.Component{
  constructor(props){
    super(props);
    console.log('common header: ' + props.notifier);
    this._updateGold = this._updateGold.bind(this);
    // let notifier = props.notifier;
    //  notifier.subscribe('GOLD_UPDATED', this._updateGold, this);
     this.state = {
       gold: 9999999,
       techPoint: 9999999
     };
  }

  _updateGold(param){
      console.log('param: ' + param);
      this.setState(Object.assign(this.state, {gold: this.state.gold + param}));
  }

  goBack(){
    FlowRouter.go('/map/zone');
  }

  render(){
    let _hidden = this.props.hideSub ? 'hide' : '';

    return(
      <header className='fadeInDown animated'>
        <section className='left'>
          <a onClick={this.goBack.bind(this)}><img src='/images/icon-button-back.svg' /></a>
        </section>
        <section className='center'>
          <h1>{this.props.headerLabel}</h1>
        </section>
        <section className='right'>
          <a><img src='/images/icon-jogging.svg' /></a>
        </section>

        <article className={_hidden}>
          <section>
            <img src='/images/icon-goldPoint.svg' />
            <span>{this.state.gold}</span>
          </section>
          <section>
            <img src='/images/icon-techPoint.svg' />
            <span>{this.state.techPoint}</span>
          </section>
        </article>
      </header>
    )
  }
}
