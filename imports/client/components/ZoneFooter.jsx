import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

export class ZoneFooter extends React.Component{
  constructor(){
    super();
  }

  render(){
    return(
      <div>
            <div className="bottomPanel wow fadeInUp" data-wow-delay="0.2s">
                <div className="bottomAreaLeft"><a href="#" id="leftMenu" /><img src="/images/icon-helicopter.svg" alt="" /></div>
                <div className="bottomAreaCenter"></div>
                <div className="bottomAreaRight"><a href="#" id="rightMenu" /><img src="/images/button-main.svg" alt="" /></div>
            </div>





            <div className="overlayRightMenu wow fadeIn" data-wow-delay="0.2s">
                <div className="rightNav">
                    <ul>
                        <li className="wow fadeInRight" data-wow-delay="0.1s"><a href="marketplace.html" /><span>Marketplace</span> <img src="/images/icon-marketplace.svg" alt="" /></li>
                        <li className="wow fadeInRight" data-wow-delay="0.2s"><a href="portfolio.html" /><span>Portfolio</span> <img src="/images/icon-portfolio.svg" alt="" /></li>
                    </ul>
                </div>
            </div>


            <div className="overlayLeftMenu wow fadeIn" data-wow-delay="0.2s">
                <div className="leftNav">
                    <ul>
                        <li className="wow fadeInLeft" data-wow-delay="0.1s"><a href="#" className="selectTransport" /><img src="/images/icon-bike.svg" alt="" /> <span>x2</span></li>
                        <li className="wow fadeInLeft" data-wow-delay="0.2s"><a href="#" className="selectTransport" /><img src="/images/icon-car.svg" alt="" /> <span>x1</span></li>
                    </ul>
                </div>
            </div>
      </div>
    );
  }
}
