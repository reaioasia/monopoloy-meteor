import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

export class CommonTabs extends React.Component{
  constructor(){
    super();

    this.state = {
      current: 'transportation'
    }
  }

  changeTab(val,event){
    console.log(val);
    //Change state
    this.setState({
      current: val
    });

    this.props.updateCurrentTab(val,this);
  }

  render(){
    let _current = this.state.current;
    let _transportation = _current == 'transportation' ? 'active' : '';
    let _building = _current == 'building' ? 'active' : '';
    let _talent = _current == 'talent' ? 'active' : '';

    console.log(_transportation);

    return(
      <div className='marketplace-tabs'>
        <ul>
          <li className={_transportation}>
            <a onClick={this.changeTab.bind(this,'transportation')}>Transportation</a>
          </li>
          <li className={_building }>
            <a onClick={this.changeTab.bind(this,'building')}>Building</a>
          </li>
          <li className={_talent}>
            <a onClick={this.changeTab.bind(this,'talent')}>Talent</a>
          </li>
        </ul>
      </div>
    )
  }
}
