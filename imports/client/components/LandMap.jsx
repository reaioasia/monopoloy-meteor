import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

import {LandPlayerPop} from './../components/LandPlayerPop.jsx';

export class LandMap extends React.Component{
  constructor(){
    super();
  }

  render(){
    return(
      <div className="gridContainer wow fadeIn" data-wow-delay="0.2s">
          <ul id="hexGrid">
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexEmpty" href="#">
                          <h4>Empty</h4>
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexNotMine" onClick={this.props.showPlayerPop.bind(this)}>
                          <div className="avatar"><img src="/images/avatar-han-solo.svg" /></div>
                          <img src="/images/icon-hex-building.svg" />
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexMine" href="#">
                          <img src="/images/icon-hex-building.svg" />
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexWater" href="#">
                          <img src="/images/icon-hex-water.svg" />
                      </a>
                  </div>
              </li>
             <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexMine" href="#">
                          <img src="/images/icon-hex-building.svg" />
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexMine" href="#">
                          <img src="/images/icon-hex-land.svg" />
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexNotMine" onClick={this.props.showPlayerPop.bind(this)}>
                          <div className="avatar"><img src="/images/avatar-han-solo.svg" /></div>
                          <img src="/images/icon-hex-building.svg" />
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexEmpty" onClick={this.props.showBuyPop.bind(this)}>
                          <h4>Empty</h4>
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexEmpty" onClick={this.props.showBuyPop.bind(this)}>
                          <h4>Empty</h4>
                      </a>
                  </div>
              </li>
               <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexElec" onClick={this.props.showPowerPop.bind(this)}>
                          <img src="/images/icon-hex-elec.svg" />
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexEmpty hexCurrent" onClick={this.props.showBuyPop.bind(this)}>
                          <h4>Empty</h4>
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexNotMine" href="#">
                          <div className="avatar"><img src="/images/avatar-princess-leia.svg" /></div>
                          <img src="/images/icon-hex-building.svg" />
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexMine" href="#">
                          <img src="/images/icon-hex-land.svg" />
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexWater" href="#">
                          <img src="/images/icon-hex-water.svg" />
                      </a>
                  </div>
              </li>
             <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexMine" href="#">
                          <img src="/images/icon-hex-building.svg" />
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexMine" href="#">
                          <img src="/images/icon-hex-building.svg" />
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexNotMine" href="#">
                          <div className="avatar"><img src="/images/avatar-princess-leia.svg" /></div>
                          <img src="/images/icon-hex-building.svg" />
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexEmpty" onClick={this.props.showBuyPop.bind(this)}>
                          <h4>Empty</h4>
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexEmpty" onClick={this.props.showBuyPop.bind(this)}>
                          <h4>Empty</h4>
                      </a>
                  </div>
              </li>
               <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexElec" onClick={this.props.showPowerPop.bind(this)}>
                          <img src="/images/icon-hex-elec.svg" />
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexEmpty" onClick={this.props.showBuyPop.bind(this)}>
                          <h4>Empty</h4>
                      </a>
                  </div>
              </li>
              <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexEmpty" onClick={this.props.showBuyPop.bind(this)}>
                          <h4>Empty</h4>
                      </a>
                  </div>
              </li>
               <li className="hex">
                  <div className="hexIn">
                      <a className="hexLink hexElec" onClick={this.props.showPowerPop.bind(this)}>
                          <img src="/images/icon-hex-elec.svg" />
                      </a>
                  </div>
              </li>
          </ul>
      </div>
    );
  }
}
