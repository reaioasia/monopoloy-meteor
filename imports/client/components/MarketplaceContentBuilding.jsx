import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

const buildingData = [
  {
    icon: '/images/icon-building.svg',
    label: 'Bunglow',
    description: '2 Land Move / 1 Area Move',
    price: 500
  },
  {
    icon: '/images/icon-building.svg',
    label: 'Bunglow',
    description: '2 Land Move / 1 Area Move',
    price: 500
  },
  {
    icon: '/images/icon-building.svg',
    label: 'Bunglow',
    description: '2 Land Move / 1 Area Move',
    price: 500
  },
  {
    icon: '/images/icon-building.svg',
    label: 'Bunglow',
    description: '2 Land Move / 1 Area Move',
    price: 500
  }
];

export class MarketplaceContentBuilding extends React.Component{
  constructor(props){
    super(props);
    this._Purchase = this._Purchase.bind(this);
    console.log('MarketplaceContentBuilding: ' + props.notifier);
  }

  _Purchase(price){
    console.log('Purchase');
    this.props.notifier.publish('GOLD_UPDATED', -price);
  }

  render(){
    let _buildings = [];

    buildingData.forEach((val,key)=>{
      console.log(val);
      let _output = (
        <li>
          <section className='icon'>
            <img src={val.icon} />
          </section>
          <section className='description'>
            <h2>{val.label}</h2>
            <h3>{val.description}</h3>
          </section>
          <section className='btnPurchase'><button onClick={()=> {this._Purchase(val.price);}}>{val.price}</button></section>
        </li>
      );

      _buildings.push(_output);
    })
    return(
      <div className='marketplace-content fadeInUp animated'>
        <ul>
          {_buildings}
        </ul>
      </div>
    )
  }
}
