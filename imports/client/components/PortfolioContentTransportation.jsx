import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

let transportationData = [
  {
    icon: '/images/icon-p-bike.svg',
    label: 'Bicycle',
    description: '2 Land Move / 1 Area Move',
    count: 5
  },
  {
    icon: '/images/icon-p-bike.svg',
    label: 'Bicycle',
    description: '2 Land Move / 1 Area Move',
    count: 5
  },
  {
    icon: '/images/icon-p-bike.svg',
    label: 'Bicycle',
    description: '2 Land Move / 1 Area Move',
    count: 5
  },
  {
    icon: '/images/icon-p-bike.svg',
    label: 'Bicycle',
    description: '2 Land Move / 1 Area Move',
    count: 5
  }
];

export class PortfolioContentTransportation extends React.Component{
  constructor(props){
    super(props);
    console.log('PortfolioContentTransportation:' + props.notifier);
    let notifier = props.notifier;
     notifier.subscribe('BICYCLE_UPDATED', this._updateBicycle, this);
     this._updateBicycle = this._updateBicycle.bind(this);
     this.setState({
       transportationData
     });
  }

  _updateBicycle(qty){
    console.log('Update bicycle:' + qty);
      transportationData.forEach((val, key) => {
          val.count = val.count + qty
      });
      this.setState(Object.assign({}, this.state, transportationData));
      console.log(this.state.transportationData);
  }

  render(){
    let _transportations = [];

    transportationData.forEach((val,key)=>{
      console.log(val);
      let _output = (
        <li>
          <section className='icon'>
            <img src={val.icon} />
          </section>
          <section className='description'>
            <h2>{val.label}</h2>
            <h3>{val.description}</h3>
          </section>
          <section className='btnPurchase'><h2>X {val.count}</h2></section>
        </li>
      );

      _transportations.push(_output);
    })
    return(
      <div className='marketplace-content fadeInUp animated'>
        <ul>
          {_transportations}
        </ul>
      </div>
    )
  }
}
