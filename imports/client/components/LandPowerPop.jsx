import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

export class LandPowerPop extends React.Component{
  constructor(){
    super();
  }

  render(){
    return(
      <div className="counterBgTimer" id="counterTimer" onClick={this.props.togglePowerPop.bind(this)}>
          <div className="counter wow fadeInDown" data-wow-delay="0.4s">
              <h2 className="timer">03:59 Left
                  <p>You're now in Power Plant</p>
              </h2>
          </div>
      </div>
    )
  }
}
