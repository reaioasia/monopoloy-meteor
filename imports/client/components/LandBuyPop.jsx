import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

export class LandBuyPop extends React.Component{
  constructor(){
    super();
  }

  render(){
    return(
      <div className="overlayPopUpPurchase wow fadeIn" data-wow-delay="0.1s" >
          <div className="popBody">
              <div className="popHeader">
                  <div className="popTitle">Purchase</div>
                  <div className="popClose"><img src="/images/icon-close.svg" onClick={this.props.toggleBuyPop.bind(this)}/></div>
              </div>
              <div className="popContent">
                  <div className="info">
                      <label>Land Worth</label>
                      <h4>200,000 Gold</h4>

                      <input type="text"  placeholder="Name Your Property" />

                      <button id="btnBuy" type="button" onClick={this.props.showBuyDonePop.bind(this)}>Buy</button>
                  </div>
              </div>
              <div className="popFooter"></div>
          </div>
      </div>
    )
  }
}
