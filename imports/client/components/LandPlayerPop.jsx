import React from 'react';
import ReactDOM,{render} from 'react-dom';

import {Meteor} from 'meteor/meteor';

export class LandPlayerPop extends React.Component{
  constructor(){
    super();
  }

  render(){
    return(
      <div className="overlayPopUpOwner wow fadeIn" data-wow-delay="0.1s">
          <div className="popBody">
              <div className="popHeader">
                  <div className="popTitle">Owner</div>
                  <div className="popClose" onClick={this.props.togglePlayerPop.bind(this)}><img src="/images/icon-close.svg" /></div>
              </div>
              <div className="popContent">
                  <div className='avatar'><img src="/images/avatar-han-solo.svg" /></div>
                  <div className="ownerName">H@nS0lo</div>
                  <div className="info">
                      <label>Land Worth</label>
                      <h4>200,000 Gold</h4>

                      <label>Building Worth</label>
                      <h4>200,000 Gold</h4>
                  </div>
              </div>
              <div className="popFooter"></div>
          </div>
      </div>
    )
  }
}
