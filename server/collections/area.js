import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

Area = new Mongo.Collection('area');

Meteor.publish('area',()=>{
  return Area.find({});
});
