import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

Land = new Mongo.Collection('land');

Meteor.publish('land',()=>{
  return Land.find({});
});
