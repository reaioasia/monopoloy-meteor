import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

Land = new Mongo.Collection('zone');

Meteor.publish('zone',()=>{
  return Land.find({});
});
