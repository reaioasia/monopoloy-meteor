import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

Vehicle = new Mongo.Collection('vehicle');

Meteor.publish('vehicle',()=>{
  return Vehicle.find({});
})
