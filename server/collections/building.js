import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

Building = new Mongo.Collection('building');

Meteor.publish('building',()=>{
  return Building.find({});
})
