import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

Marketplace = new Mongo.Collection('marketplace');

Meteor.publish('marketplace',()=>{
  return Marketplace.find({});
})
